import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import Song from './song';
import SongService from './song.service';

@Component({
  selector: 'songs',
  templateUrl: './song.component.html',
  providers: [ SongService ]
})

export default class SongComponent {

}
