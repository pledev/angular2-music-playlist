import Category from '../category/category';

export default class Song {
  _id: string;
  name: string;
  encodeName: string;
  extension: string;
  username: string;
  updatedAt: string;
  createdAt: string;
  category: Category;
}
