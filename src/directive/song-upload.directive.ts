import { Directive, ElementRef, HostListener, Input, Renderer } from '@angular/core';

@Directive({
  selector: '[song-upload]'
})

export default class SongUploadDirective {
  private uploadFile: any;

  constructor(private el: ElementRef, private renderer: Renderer) {}

  @HostListener('onchange') onChange() {
    this.uploadFile = this.el.nativeElement.files;
    console.log(this.uploadFile);
  }
}
