import { NgModule } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AUTH_PROVIDERS } from 'angular2-jwt';

import { routing } from './app.routing';

import AppComponent from './app.component';
import CategoryComponent from '../category/category.component';
import MediaPlayerComponent from '../media-player/media-player.component';
import PlaylistComponent from '../playlist/playlist.component';
import SongComponent from '../song/song.component';

import SongUploadDirective from '../directive/song-upload.directive';

import AuthService from '../auth/auth.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  declarations: [
    AppComponent,
    CategoryComponent,
    MediaPlayerComponent,
    PlaylistComponent,
    SongComponent,
    SongUploadDirective
  ],
  providers: [
    AuthService,
    AUTH_PROVIDERS
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
