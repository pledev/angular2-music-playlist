import { Component, OnInit } from '@angular/core';
import AuthService from '../auth/auth.service';

import '../../public/css/styles.css';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
})

export default class AppComponent {
  title = 'Collections';

  constructor(private authService: AuthService) {}
}
