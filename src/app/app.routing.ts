import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import AppComponent from './app.component';
import PlaylistComponent from '../playlist/playlist.component';
import CategoryComponent from '../category/category.component';
import SongComponent from '../song/song.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/playlist',
    pathMatch: 'full'
  },
  {
    path: 'playlist',
    component: PlaylistComponent
  },
  {
    path: 'category',
    component: CategoryComponent
  }
]

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
