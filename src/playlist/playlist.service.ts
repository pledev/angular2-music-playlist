import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import Playlist from './playlist';

@Injectable()
export default class PlaylistService {
  private playlistUrl = 'api/playlist';

  constructor(private http: Http) {}

  getPlaylists(): Observable<Playlist[]> {
    return this.http.get(this.playlistUrl)
      .map(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body.data || {}
  }

  private handleError(error: any) {
    let errMsg = (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.log(errMsg);
    return Observable.throw(errMsg);
  }
}
