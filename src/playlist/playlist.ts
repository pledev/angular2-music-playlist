import Song from '../song/song';

export default class Playlist {
  id: number;
  name: string;
  songs: Song[];
}
