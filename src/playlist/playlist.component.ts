import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import Playlist from './playlist';
import PlaylistService from './playlist.service';
import AuthService from '../auth/auth.service';

@Component({
  selector: 'playlists',
  templateUrl: './playlist.component.html',
  providers: [ PlaylistService ]
})

export default class PlaylistComponent implements OnInit {
  playlists: Playlist[];
  errorMessage: string;
  selectedPlaylist: Playlist;
  currentPlaylist: Playlist;

  constructor(private playlistService: PlaylistService,
    private router: Router,
    private authService: AuthService) {

    }

  getPlaylists(): void {
    this.playlistService.getPlaylists().subscribe(
      playlists => this.playlists = playlists,
      error => this.errorMessage = error
    );
  }

  viewPlaylist(playlist: Playlist): void {
    if(this.selectedPlaylist && this.selectedPlaylist.id === playlist.id) {
      this.selectedPlaylist = null;
    } else {
      this.selectedPlaylist = playlist;
    }
  }

  play(playlist: Playlist): void {
    this.currentPlaylist = playlist;
  }

  createPlaylist(): void {

  }

  ngOnInit(): void {
    this.getPlaylists();
  }
}
