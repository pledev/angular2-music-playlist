import { Injectable } from '@angular/core';
import { tokenNotExpired } from 'angular2-jwt';

declare var Auth0Lock: any;

export default class AuthService {

  lock = new Auth0Lock('sKnLiqpASudnmt46A32iKmorEKdd4Ub1', 'pledev.auth0.com', {
    languageDictionary: {
      title: "Music Storage"
    }
  });
  isAdmin: boolean;
  userProfile: any;

  constructor() {
    this.userProfile = JSON.parse(localStorage.getItem('profile'));
    this.lock.on("authenticated", (authResult: any) => {
      localStorage.setItem('id_token', authResult.idToken);
      this.lock.getProfile(authResult.idToken, (error: any, profile: any) => {
        if(error) {
          throw error;
        }
        localStorage.setItem('profile', JSON.stringify(profile));
        this.userProfile = profile;
        if(this.userProfile.user_metadata &&
          this.userProfile.user_metadata.role && this.userProfile.user_metadata.role == 'admin') {
          this.isAdmin = true;
        }
      });
    });
  }

  public login(): void {
    this.lock.show();
  }

  public authenticated() {
    return tokenNotExpired();
  }

  public logout(): void {
    localStorage.removeItem('id_token');
    localStorage.removeItem('profile');
    this.userProfile = undefined;
  }
}
