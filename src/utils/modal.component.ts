import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'modal',
  templateUrl: './modal.component.html'
})

export default class ModalComponent implements OnInit {
  @ViewChild('modal', {}) modal: ViewContainerRef;

  ngOnInit(): void {

  }
}
