import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import Category from './category';

@Injectable()
export default class CategoryService {
  private categoryUrl = 'api/category';
  private songUrl = 'api/song';

  constructor(private http: Http) {}

  getCategories(): Observable<Category[]> {
    return this.http.get(this.categoryUrl)
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  getSongByCategories() {
    return Observable.forkJoin([
      this.http.get(this.categoryUrl).map(this.extractData).catch(this.handleError),
      this.http.get(this.songUrl).map(this.extractData).catch(this.handleError)
    ]);
  }

  addNewCategory(name: string): Observable<Category> {
    let body = JSON.stringify({ name });
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(`${this.categoryUrl}/add`, body, options)
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body.data || {}
  }

  private handleError(error: any) {
    let errMsg = (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.log(errMsg);
    return Observable.throw(errMsg);
  }
}
