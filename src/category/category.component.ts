import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import Category from './category';
import Song from '../song/song';

import AuthService from '../auth/auth.service';
import CategoryService from './category.service';

import SongUploadDirective from '../directive/song-upload.directive';

@Component({
  selector: 'categories',
  templateUrl: './category.component.html',
  styleUrls: [ './category.component.css' ],
  providers: [
    CategoryService
  ]
})

export default class CategoryComponent implements OnInit {
  private songsByCategories: Array<any> = [];
  private staticSongDir: string = '/static/audios/';
  private categories: Category[];
  private newCategory: string;
  uploadFile: File;
  selectedCategory: Category;
  currentSongDir: string;
  errorMessage: string;
  private addCategoryErrorMessage: string;
  private isOpenImportForm: boolean = false;
  private isOpenAddCategoryForm: boolean = false;
  @ViewChild('songUpload') songUpload: ElementRef;
  @ViewChild('mediaPlayer') mediaPlayer: ElementRef;

  constructor(
    private categoryService: CategoryService,
    private authService: AuthService) {

    }

  onChange(event: any): void {
    this.uploadFile = <File> event.target.files[0];
  }

  getSongByCategories(): void {
    // this.categoryService.getSongByCategories().subscribe(
    //   data => {
    //     let categories: Category[] = data[0];
    //     let songs: Song[] = data[1];
    //
    //     this.categories = categories;
    //
    //     categories.forEach((category: Category) => {
    //       this.songsByCategories.push({
    //         category: category,
    //         songs: []
    //       })
    //     });
    //
    //     songs.forEach((song: Song) => {
    //       let categoryId = song.category.id;
    //       this.songsByCategories.forEach((songsByCategory: any) => {
    //         if(songsByCategory.category.id == categoryId) {
    //           return songsByCategory.songs.push(song);
    //         }
    //       })
    //     });
    //   },
    //   error => this.errorMessage = error
    // );
  }

  openImportForm(): void {
    this.isOpenImportForm = (this.isOpenImportForm == false) ? true : false;
  }

  openCategoryForm(): void {
    this.isOpenAddCategoryForm = (this.isOpenAddCategoryForm == false) ? true: false;
  }
  addCategory(): void {
    if(this.newCategory.length == 0) {
      return ;
    }

    this.categoryService.addNewCategory(this.newCategory).subscribe(
      (category: Category) => {
        this.categories.push(category);
        this.newCategory = undefined;
      },
      (error: any) => this.addCategoryErrorMessage = error
    );
  }

  upload() {
    let url = 'http://' + window.location.host + '/api/category';
    let options: Array<string> = [];
    this.makeFileRequest(url, options, {
      file: this.uploadFile,
      category: this.selectedCategory
    }).then((result: any) => {
      let songJson = result.data;
      let categoryId = songJson.category.id;
      this.songsByCategories.forEach((songsByCategory: any) => {
        if(songsByCategory.category.id == categoryId) {
          return songsByCategory.songs.push({
              _id: songJson._id,
              name: songJson.name,
              encodeName: songJson.encodeName,
              extension: songJson.extension,
              username: songJson.username,
              updatedAt: songJson.updatedAt,
              createdAt: songJson.createdAt,
              category: {
                id: songJson.category.id,
                name: songJson.category.name
              }
          });
        }
      });
      this.uploadFile = undefined;
      this.selectedCategory = undefined;
    }, (error: any) => {
      this.errorMessage = JSON.parse(error).errmsg;
    });

  }

  makeFileRequest(url: string, options: Array<string>, data: any) {
    return new Promise((resolve, reject) => {
      let formData: any = new FormData();
      let xhr = new XMLHttpRequest();

      formData.append("file", data.file);
      formData.append('category', JSON.stringify(data.category));

      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }

      xhr.open('POST', url, true);
      xhr.send(formData);
    })
  }

  playSong(song: Song): void {
    this.currentSongDir = `${this.staticSongDir}/${song.encodeName}.${song.extension}`;
    this.mediaPlayer.nativeElement.load();
    this.mediaPlayer.nativeElement.play();
  }

  ngOnInit() {
    // this.getSongByCategories();
    this.categoryService.getCategories().subscribe(
      (categories: Category[]) => this.categories = categories,
      (error: any) => this.errorMessage = error
    )
  }
}
