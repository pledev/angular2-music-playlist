import Song from '../song/song';

export default class Category {
  _id: string;
  name: string;
  songs: Song[];
}
