import { Component, Input, ViewChild, ElementRef, AfterViewInit, OnInit } from '@angular/core';

import AuthService from '../auth/auth.service';

import Playlist from '../playlist/playlist';
import Song from '../song/song';

@Component({
  selector: 'media-player',
  templateUrl: './media-player.component.html',
  styleUrls: [ './media-player.component.css' ]
})

export default class MediaPlayerComponent implements OnInit {
  uploadFiles: Array<File>;
  private defaultDir = "/static/audios";

  private isLoopSelected: number;
  private loopList: Array<number> = [];

  private isShuffle: number;
  private playedList: Array<number> = [];

  private currentSong: Song;

  private lastPlaylistId: number = -1;
  private status : string;

  songDir: string;
  @ViewChild('player') player: ElementRef;
  _currentPlaylist: Playlist;

  constructor(private authService: AuthService) {}

  @Input()
  set currentPlaylist(currentPlaylist: Playlist) {
    if(currentPlaylist) {
      if(this.lastPlaylistId == -1) {
        this.lastPlaylistId = currentPlaylist.id;
      }
      this._currentPlaylist = currentPlaylist;
    }
  }

  play(song: Song): void {
    this.resetOnPlay();
    let audio = this.player.nativeElement;
    this.status = "Playing";
    this.currentSong = song;
    this.songDir = `${this.defaultDir}/${this._currentPlaylist.name}/${this.currentSong.name}`;
    audio.load();
    audio.play();
  }

  getNext(): void {
    // this.resetOnPlay();
    // let songs = this._currentPlaylist.songs;
    // let currentSongId = this.currentSong._id;
    // let nextSongId = currentSongId + 1;
    //
    // if(this.isLoopSelected == 1) {
    //   let loopIndex = this.loopList.indexOf(currentSongId);
    //   let nextLoopIndex = -1;
    //   if(loopIndex == -1) {
    //     nextLoopIndex = 0;
    //   } else {
    //     nextLoopIndex = loopIndex + 1;
    //     if(nextLoopIndex == this.loopList.length) {
    //       nextLoopIndex = 0;
    //     }
    //   }
    //
    //   nextSongId = this.loopList[nextLoopIndex];
    // } else {
    //   if(!songs.find((song) => song._id == nextSongId)) {
    //     nextSongId = 1;
    //   }
    // }
    // this.currentSong = songs.find((song) => song._id == nextSongId );
    // this.songDir = `${this.defaultDir}/${this._currentPlaylist.name}/${this.currentSong.name}`;
    // this.player.nativeElement.load();
    // this.player.nativeElement.play();
  }

  getPrev(): void {
    // this.resetOnPlay();
    // let songs = this._currentPlaylist.songs;
    // let currentSongId = this.currentSong._id;
    // let nextSongId = currentSongId - 1;
    //
    // if(this.isLoopSelected == 1) {
    //   let loopIndex = this.loopList.indexOf(currentSongId);
    //   let nextLoopIndex = -1;
    //   if(loopIndex == -1) {
    //     nextLoopIndex = 0;
    //   } else {
    //     nextLoopIndex = loopIndex - 1;
    //     if(nextLoopIndex == -1) {
    //       nextLoopIndex = this.loopList.length - 1;
    //     }
    //   }
    //   nextSongId = this.loopList[nextLoopIndex];
    // } else {
    //   if(!songs.find((song) => song._id == nextSongId)) {
    //     nextSongId = songs.length - 1;
    //   }
    // }
    // this.currentSong = songs.find((song) => song._id == nextSongId);
    // this.songDir = `${this.defaultDir}/${this._currentPlaylist.name}/${this.currentSong.name}`;
    // this.player.nativeElement.load();
    // this.player.nativeElement.play();
  }

  onPause(): void {
    this.status = "Paused";
  }

  onPlay(): void {
    this.status = "Playing"
  }

  onChange(event: any): void {
    this.uploadFiles = <Array<File>> event.target.files;
  }

  upload() {
    let url = 'http://' + window.location.host + '/upload/' + this._currentPlaylist.name;
    let options: Array<string> = [];
    this.makeFileRequest(url, options, this.uploadFiles).then((result) => {

      let uploadedFiles = <Array<any>> result;
      uploadedFiles.forEach((file, index) => {
        let songs = this._currentPlaylist.songs;
        // if(!songs.find((song) => song.name === file.filename)) {
        //   this._currentPlaylist.songs.push({
        //     id: songs.length + 1,
        //     name: file.filename,
        //     time: ''
        //   });
        // }
      });
    }, (error) => {
      console.log(error);
    });
    this.uploadFiles = null;
  }

  makeFileRequest(url: string, options: Array<string>, files: Array<File>) {
    return new Promise((resolve, reject) => {
      let formData: any = new FormData();
      let xhr = new XMLHttpRequest();

      for(var i = 0; i < files.length; i++) {
          formData.append("uploads[]", files[i], files[i].name);
      }

      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }

      xhr.open('POST', url, true);
      xhr.send(formData);
    })
  }

  addLoopList(event: any): void {
    this.resetOnPlay();
    let songId = event.target.value;
    let autoStart = (this.loopList.length == 0) ? true: false;
    let loopIndex = this.loopList.indexOf(songId);

    if(loopIndex == -1) {
      this.loopList.push(songId);
    } else {
      this.loopList.slice(loopIndex, 1);
    }

    if(autoStart) {
      this.status = "Playing";
      this.currentSong= this._currentPlaylist.songs.find((song) => song._id == songId)
      this.songDir = `${this.defaultDir}/${this._currentPlaylist.name}/${this.currentSong.name}`;
      this.player.nativeElement.load();
      this.player.nativeElement.play();
    }

    this.loopList.sort((a,b) => a-b);
  }

  resetOnPlay(): void {
    if(this.lastPlaylistId != this._currentPlaylist.id) {
      this.loopList = [];
      this.lastPlaylistId = this._currentPlaylist.id;
    }
    if(this._currentPlaylist.songs.length <= 1) {
      this.isLoopSelected = undefined;
    }
  }

  ngOnInit() {
    this.status = "Paused";
  }
}
