var express = require('express'),
    Song = require('../models/song'),
    router = express.Router();

router.get('/', function(req, res) {
  Song.find().exec(function(error, songs) {
    if(error) {
      res.status(500).send(error);
    } else {
      console.log(songs);
      res.json({
        data: songs
      })
    }
  });
});

module.exports = router;
