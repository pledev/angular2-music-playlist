var express = require('express'),
    fs = require('fs'),
    path = require('path'),
    router = express.Router();

const defaultDir = path.resolve(__dirname + '/../../public/audios/');

router.get('/', function(req, res) {
  var playlists = getPlayLists(defaultDir);

  playlists.forEach(function(playlist) {
    playlist.songs = getSongs(playlist, defaultDir);
  });

  res.json({
    data: playlists
  });
});

var getPlayLists = function(srcPath) {
  var playlists = [];
  var index = 1;

  fs.readdirSync(srcPath).filter(function(file) {
    if(fs.statSync(path.join(srcPath, file)).isDirectory()) {
      playlists.push({
        id: index,
        name: file,
        songs: []
      });
      index++;
    }
  });

  return playlists;
}

var getSongs = function(playlist, srcPath) {
  var playlistPath = path.resolve(srcPath + '/' + playlist.name);
  var songs = [];
  var files = fs.readdirSync(playlistPath);
  files.forEach(function(file, index) {
    if(file.includes('.mp3') || file.includes('.m4a')) {
      songs.push({
        id: index,
        name: file,
        time: '3:00'
      });
    }
  });
  return songs;
}

module.exports = router;
