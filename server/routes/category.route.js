var express = require('express'),
    multer = require('multer'),
    crypto = require('crypto'),
    path = require('path'),
    router = express.Router();

var Song = require('../models/song');
var Category = require('../models/category');

var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, path.resolve(__dirname + '/../../public/audios/'));
  },
  filename: function (req, file, callback) {
    var fileName = file.originalname.substr(0, file.originalname.lastIndexOf('.'));
    var extension = file.originalname.substr(file.originalname.lastIndexOf('.'));
    var encodeName = crypto.createHash('md5').update(fileName).digest('hex');
    callback(null, encodeName + extension);
  }
});

router.get('/', function(req, res) {
  Category.find().exec(function(error, categories) {
    if(error) {
      res.status(500).send(error);
    } else {
      res.json({
        data: categories
      })
    }
  });
});

router.post('/', multer({ storage: storage }).single('file'), function(req, res) {
  var category = JSON.parse(req.body.category);
  var extension = req.file.originalname.substr(req.file.originalname.lastIndexOf('.') + 1);

  const newSong = new Song({
    name: req.file.originalname.slice(0, -4),
    encodeName: req.file.filename.slice(0, -4),
    extension: extension,
    username: 'admin',
    category: category
  });

  newSong.save(function(err, saved) {
    if (err) {
      res.status(500).send(err);
    } else {
      res.json({
        data: saved
      })
    }
  });
});

router.post('/add', function(req, res) {
  const newCategory = new Category({
    name: req.body.name,
    songs: []
  });

  newCategory.save(function(error, saved) {
    if(error) {
      res.status(500).send(error);
    } else {
      res.json({
        data: saved
      });
    }
  });
});

module.exports = router;
