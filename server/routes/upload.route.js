var express = require('express'),
    multer = require('multer'),
    path = require('path');

var router = express.Router();
var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    var playlistName = req.params.playlistName;
    callback(null, path.resolve(__dirname + '/../../public/audios/' + playlistName));
  },
  filename: function (req, file, callback) {
    callback(null, file.originalname);
  }
});

router.post('/:playlistName', multer({ storage: storage }).array("uploads[]", 12), function(req, res) {
  res.send(req.files);
});

module.exports = router;
