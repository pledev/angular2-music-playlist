var mongoose = require('mongoose');

var playlistSchema = new mongoose.Schema({
  songs: { type: 'Mixed' },
  creator: { type: 'String', required: true },
  createdAt: { type: 'Date', default: Date.now, required: true },
  updatedAt: { type: 'Date', default: Date.now, required: true }
});

module.exports = mongoose.model('Playlist', playlistSchema);
