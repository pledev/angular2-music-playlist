var mongoose = require('mongoose');

var songSchema = new mongoose.Schema({
  name: { type: 'String', required: true },
  encodeName: { type: 'String', required: true, index: { unique: true } },
  username: { type: 'String', min: 8, max: 32 },
  extension: { type: 'String', required: true },
  category: { type: 'Mixed', required: true },
  createdAt: { type: 'Date', default: Date.now, required: true },
  updatedAt: { type: 'Date', default: Date.now, required: true }
}, {
  collection: 'songs'
});

module.exports = mongoose.model('Song', songSchema);
