var mongoose = require('mongoose');

var categorySchema = new mongoose.Schema({
  name: { type: 'String', required: true },
  songs: [{ type: 'Mixed' }]
});

module.exports = mongoose.model('Category', categorySchema);
