var express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    mongoose = require('mongoose'),
    morgan = require('morgan'),
    config = require('./config');

var app = express();
app.set('port', process.env.PORT || 3000);

mongoose.Promise = global.Promise;
mongoose.connect(config.mongoURL, function(error) {
  if(error) {
    console.log(error);
    throw error;
  }
});

app.use('/build', express.static(path.resolve(__dirname + '/../dist')));
app.use('/static', express.static(path.resolve(__dirname + '/../public')));

app.use(morgan('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var playlistRouter = require('./routes/playlist.route');
var uploadRouter = require('./routes/upload.route');
var categoryRouter = require('./routes/category.route');
var songRouter = require('./routes/song.route');

app.use('/api/playlist', playlistRouter);
app.use('/api/category', categoryRouter);
app.use('/api/song', songRouter);
app.use('/upload', uploadRouter);

app.get('/*', function(req, res) {
  res.sendFile(path.join(__dirname, '../dist/index.html'))
});

app.listen(app.get('port'), function() {
  console.log('Server is listening on port ' + app.get('port'));
});

module.exports = app;
